# Installing charmed Kubeflow on microk8s

## Install and prepare MicroK8s

**Step 1:** MicroK8s is installed from a snap package. The published snap maintains different channels for different releases of Kubernetes.

`sudo snap install microk8s --classic --channel=1.24/stable`

**Step 2:** For MicroK8s to work without having to use sudo for every command, it creates a group called microk8s. To make it more convenient to run commands, you will add the current user to this group:

`sudo usermod -a -G microk8s $USER`

`newgrp microk8s`

**Step 3:** It is also useful to make sure the user has the proper access and ownership of any kubectl configuration files:

`sudo chown -f -R $USER ~/.kube`

**Step 4:** Enable the following Microk8s add-ons to configure your Kubernetes cluster with extra services needed to run Charmed Kubeflow.

`microk8s enable dns storage ingress metallb:10.64.140.43-10.64.140.49`

**Step 5:** We’ve now installed and configured MicroK8s. It will start running automatically, but can take 5 minutes or so before it’s ready for action.
Run the following command to tell MicroK8s to report its status to us when it’s ready:

`microk8s status --wait-ready`

## Install and deploy Juju
Juju is an operation Lifecycle manager (OLM) for clouds, bare metal or Kubernetes. We will be using it to deploy and manage the components which make up Kubeflow.

**Step 1:** To install Juju from snap, run this command:

`sudo snap install juju --classic --channel=2.9/stable`

**Step 2:** Run the following command to deploy a Juju controller to the Kubernetes we set up with MicroK8s:

`juju bootstrap microk8s`

**Step 3:** To add a model for Kubeflow to the controller. Run the following command to add a model called kubeflow:

`juju add-model kubeflow`

**Step 4:** Deploy Charmed Kubeflow: Run this code to deploy the Charmed Kubeflow bundle with Juju. This step takes a long time, continue monitoring the setup for completion with juju status command. 

`juju deploy kubeflow --trust  --channel=1.7/stable`

**Step 5:** Run the following command to keep a watch on the components which are not active yet:

`watch -n 1 -c juju status --color`

**Step 6:** Configure Dashboard Access: In order to access kubeflow through its dashboard service, we’ll need to configure the bundle a bit so that it supports authentication and authorization. To do so, run these commands:

`juju config dex-auth public-url=http://10.64.140.43.nip.io`

`juju config oidc-gatekeeper public-url=http://10.64.140.43.nip.io`


**Step 7:** To enable simple authentication, and set a username and password for your Kubeflow deployment, run the following commands:

`juju config dex-auth static-username=admin`

`juju config dex-auth static-password=admin`

## Set up remote access
**Step 1:** To check the status if all services are active.
`watch -c juju status –color`

**Step 2:** If every service is active check in the browser whether kubeflow dashboard is loading http://10.64.140.43.nip.io

**Step 3:** Exposing the ClusterIp by using StaticIP: Since the clusterip service is utilized within the microk8s cluster and we need to expose the kubeflow-dashboard, the service type needs be changed from ClusterIP to NodePort.

To check the service type of microk8s kubeflow-dashboard

`sudo microk8s kubectl get svc -A|grep kubeflow-dashboard`

![Javatpoint](https://ignitariumtech-my.sharepoint.com/:i:/g/personal/shinta_zachariah_ignitarium_com/ESqkWj6jKqtOqBEEW_99u14BY0y8tn-xmvLjWthbOBpi8g)  

Run the following command to descibe the service kubeflow-dashboard

`sudo microk8s kubectl describe svc kubeflow-dashboard -n kubeflow`

Edit and save the kubeflow-dashboard service document and change service type from ClusterIP to NodePort

`sudo microk8s kubectl edit svc kubeflow-dashboard -n kubeflow`

Check whether the Type is changed

`sudo microk8s kubectl describe svc kubeflow-dashboard -n kubeflow`



